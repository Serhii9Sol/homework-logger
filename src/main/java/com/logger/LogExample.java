package com.logger;

import com.logger.exceptions.MyException;
import com.logger.services.RandomNumberService;
import org.apache.log4j.Logger;
import java.util.Random;

public class LogExample {

    private static final String MSG_SUCCESSFUL_COMPLETE = "Приложение успешно запущено";
    private final Logger logConsole;
    private final Logger logFile;

    public LogExample(Logger logConsole, Logger logFile) {
        this.logConsole = logConsole;
        this.logFile = logFile;
    }

    public static void main(String[] args) {
        LogExample logExample = new LogExample(Logger.getLogger("loggerConsole"), Logger.getLogger("loggerFile"));
        logExample.generate(new RandomNumberService(new Random()), 50);
    }

    public void generate(RandomNumberService randomNumberService, int count) {
        for (int i = 0; i < count; i++) {
            try {
                randomNumberService.getRandomNumber();
                logConsole.info(MSG_SUCCESSFUL_COMPLETE);
                logFile.info(MSG_SUCCESSFUL_COMPLETE);
            } catch (MyException e) {
                logConsole.info(e.getMessage());
                logFile.info(e.getMessage());
            }
        }
    }
}
