package com.logger.services;

import com.logger.exceptions.MyException;
import java.util.Random;

public class RandomNumberService {

    private Random random;

    public RandomNumberService(Random random) {
        this.random = random;
    }

    public int getRandomNumber() throws MyException {
        int randomNum = random.nextInt(10 - 0 + 1);
        if (randomNum <= 5) {
            throw new MyException(String.valueOf(randomNum));
        }
        return randomNum;
    }
}
