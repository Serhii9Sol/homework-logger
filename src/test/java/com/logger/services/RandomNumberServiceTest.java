package com.logger.services;

import com.logger.exceptions.MyException;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import java.util.Random;
import static org.junit.jupiter.api.Assertions.*;

class RandomNumberServiceTest {

    private final Random random = Mockito.mock(Random.class);
    private final RandomNumberService cut = new RandomNumberService(random);

    @Test
    void getRandomNumberLessThanSixTest() {
        int generatedNumber = 1;
        Mockito.when(random.nextInt(11)).thenReturn(generatedNumber);

        assertThrowsExactly(MyException.class, cut::getRandomNumber,
                "Сгенерированное число – " + generatedNumber);
    }

    @Test
    void getRandomNumberMoreThanFifeTest() throws MyException {
        int expected = 7;
        Mockito.when(random.nextInt(11)).thenReturn(expected);

        int actual = cut.getRandomNumber();

        assertEquals(expected, actual);
    }

}