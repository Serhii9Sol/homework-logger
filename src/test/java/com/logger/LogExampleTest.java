package com.logger;

import com.logger.services.RandomNumberService;
import org.apache.log4j.Logger;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import java.util.List;
import java.util.Random;


class LogExampleTest {

    private static final String MSG_SUCCESSFUL_COMPLETE = "Приложение успешно запущено";
    private static final String MSG_EXCEPTION = "Сгенерированное число – ";
    private final Random random = Mockito.mock(Random.class);
    private final Logger logConsole = Mockito.mock(Logger.class);
    private final Logger logFile = Mockito.mock(Logger.class);
    private final RandomNumberService rns = new RandomNumberService(random);
    private final LogExample cut = new LogExample(logConsole, logFile);


    static List<Arguments> generateTestArgs() {
        return List.of(
                Arguments.arguments(4 , 1, MSG_EXCEPTION + 4),
                Arguments.arguments(6 , 1, MSG_SUCCESSFUL_COMPLETE)
        );
    }

    @ParameterizedTest
    @MethodSource("generateTestArgs")
    void generateTest(int generatedNumber, int countOfGenerating, String message) {
        Mockito.when(random.nextInt(11)).thenReturn(generatedNumber);

        cut.generate(rns, countOfGenerating);

        Mockito.verify(logConsole, Mockito.times(1)).info(message);
        Mockito.verify(logFile, Mockito.times(1)).info(message);
    }
}